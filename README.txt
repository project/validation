
Creates a block in the sidebar displaying validation tool buttons:

XML Validator
http://validator.w3.org

CSS Validator
http://jigsaw.w3.org/css-validator

Accessibility Validator
http://www.cynthiasays.com/org/cynthiatested.htm

When a button is pressed the page is validated and a report is shown.

This is useful:

    * Development stage when working on a theme
    * Content input stage, to check the content is marked up to be standards compliant and accessible
    * Comment stage
    * Making visitors aware of standards and accessibility


=======
Maintainer
=======

Robert Castelo
services@cortextcommunications.com
